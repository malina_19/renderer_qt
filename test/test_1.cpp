#include "gtest/gtest.h"
#include "../triangle.h"

TEST(test_1, ok)
{
      Triangle *tr;
      tr = new Triangle(Point(3.0, 3.0, 15.0), Point(-3.0, -3.0, 25.0), Point(4.0, 5.0, 5.0));
      Point vec_1 = tr->form_vector(tr->points_3D_[0], tr->points_3D_[1]);
      Point vec_2 = tr->form_vector(tr->points_3D_[1], tr->points_3D_[2]);
      Point vec_3 = tr->form_vector(tr->points_3D_[0], tr->points_3D_[2]);
      Point normal = tr->normal();
      float angle_1 = tr->angle(vec_1, normal);
      float angle_2 = tr->angle(vec_2, normal);
      float angle_3 = tr->angle(vec_3, normal);
      ASSERT_EQ(angle_1 + angle_2 + angle_3, 270);
//      delete(tr);

}

 TEST(test_2, ok) {
//   qDebug() << "test!";
     Triangle *tr;
     tr = new Triangle(Point(4, 600, 2.1), Point(20, 500, 8.1), Point(800, 600, 15.1));
//     tr = new Triangle(Point(3.0, 3.0, 15.0), Point(-3.0, -3.0, 25.0), Point(4.0, 5.0, 5.0));
     Point vec_1 = tr->form_vector(tr->points_3D_[1], tr->points_3D_[0]);
     Point vec_2 = tr->form_vector(tr->points_3D_[2], tr->points_3D_[0]);
     Point vec_3 = tr->form_vector(tr->points_3D_[0], tr->points_3D_[2]);
     Point vec_4 = tr->form_vector(tr->points_3D_[1], tr->points_3D_[2]);
     Point vec_5 = tr->form_vector(tr->points_3D_[0], tr->points_3D_[1]);
     Point vec_6 = tr->form_vector(tr->points_3D_[2], tr->points_3D_[1]);
     float angle_A = tr->angle(vec_1, vec_2);
     float angle_B = tr->angle(vec_3, vec_4);
     float angle_C = tr->angle(vec_5, vec_6);
     ASSERT_EQ(angle_A + angle_B + angle_C, 180);
 }

 TEST(test_3, ok) {
//   qDebug() << "test!";
     Triangle *tr;
     Point vec_1 = Point(-4.0, -5.0, 2.0);
     Point vec_2 = Point(4.0, 1.0, -5.0);
     float angle = tr->angle(vec_1, vec_2);
     ASSERT_EQ(round( angle * 1000.0 ) / 1000.0, 135.485);
 }

// TEST(test_4, ok) {
//     Ui::MainWindow *ui;
//     Scene *scene;
//     Triangle *triangle_;
//     scene = new Scene(0, 0, 800, 800, this);
//     ui->graphicsView->setScene(scene);
//     Triangle *tr;
//     tr = new Triangle(Point(4, 600, 2.1), Point(20, 500, 8.1), Point(800, 600, 15.1));

//     ASSERT_EQ(angle, 135.485);
// }
