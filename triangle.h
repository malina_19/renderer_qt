#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "point.h"

class Triangle : public Point
{
public:
    Triangle();
    Triangle(Point point1, Point point2, Point point3);
    ~Triangle();
    void print();
//    void rotate_around_z(double angle);
//    void rotate_around_y(double angle);
//    void rotate_around_x(double angle);
    Point points_3D_[3];
    Point normal();
    Point center();
//    Point form_vector(Point terminal, Point initial);
    Point normalize_vector(Point vec);
    float angle(Point vec1, Point vec2);
    void move(Point move_point, Point move_to);
private:
    Point points_3D[3];
    Point cross_product(Point vector_1,Point vector_2);
    Point normal_, center_;

};

#endif // TRIANGLE_H
