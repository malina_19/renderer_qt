#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QDebug>

#include "scene.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void light_circle_test(Point center, float radius, float delta, int step);
    void light_circle_test_1(Point center, float radius, float delta, int step);
    void pot_cirlce_test(Point center, float radius, float delta, int step);
    void pot_cirlce_test_1(Point center, float radius, float delta, int step);
    void pot_around_axis_test(Point center, float delta, int step);
private:
    void create_picture(int step);
private:
    Ui::MainWindow *ui;
    Scene *scene;
    Point light;
    Triangle *triangle_, *triangle, *triangle_1, *triangle_2;
    Model *model;
    QTimer *animationTimer;
private slots:
    void rePaint();
};
#endif // MAINWINDOW_H
