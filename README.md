# Project Description
This is rendering project written using framework Qt

![](anim1.gif)

# Project history
```
Final goal was to render rotation of Utah teapot.
1) Part 1/ C
Project started with basic algorithms of drawing points, lines using array filled with symbols. 
Using the same array function of filling the triangle in plane was written. 
Then libattopng library to create PNG images was added.
2) Part 2/ C++ 
After project was rewritten in C++, for creating animation of rotation Qt was used.

Firstly point class was added in order to simplify the work with points of future model in 3D space.
Secondly triangle class was added in order to manage the geometric work with triangles: methods for
creacting vector, normalization of vector, estimation of angle between the vectors, estimation of
normal and center of triangle were written.

Thirdly the class for rendering scene was created. It uses several Qt classes to manage working with:
- 2D projection of the 3D model (The QGraphicsScene class);
- graphical items in a QGraphicsScene, more specifically polygons to render triangles on the 2D canvas
(The QGraphicsItem class).
Plane, light source and camera were added as points.
Method Scene::shading(Triangle triangle, QColor color, Point light) was used to calculate the color of
each triangle according to angle between triangle normal and light vector

```
