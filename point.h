#ifndef POINT_H
#define POINT_H

#include <cmath>
#include <algorithm>
#include <iostream>


class Point
{
public:
    Point();
    Point(const float& x, float y, float z):x_(x), y_(y), z_(z){};
    /*const*/ float x() const {return x_;}
    void set_x(int x){x_ = x;}
    /*const*/ float y() const {return y_;}
    void set_y(int y){y_ = y;}
    /*const*/ float z() const {return z_;}
    void set_z(int z){z_ = z;}
    void print();
    Point form_vector(Point terminal, Point initial);
private:
    float x_, y_, z_;
};

#endif // POINT_H
