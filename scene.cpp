﻿#include "scene.h"

Scene::Scene(qreal x, qreal y, qreal width, qreal height, QObject *parent)
{
    width_ = width;
    height_ = height;
    camera = Point(0, 0, 0);
    plane = Point(0, 0, 1);
    color_=random_color();
    set_light_source(Point(0, 0, 10));
}

Scene::Scene()
{

}
QPoint Scene::switch_to_pixel(Point p)
{
    QPoint point;

//    p.print();
    float l, m, n;
    l = p.x() - camera.x();
    m = p.y() - camera.y();
    n = p.z() - camera.z();
    point.setX( (l/n) * (plane.z() - camera.z()) + camera.x() );
    point.setY( (m/n) * (plane.z() - camera.z()) + camera.y() );
//    point = normalize(point, 10000);

    point.setX(p.x()+width()/2);
    point.setY(p.y()+height()/2);

//    p.print();
//    set_projection_matrix();
//    p = matrix_multiplicator(p, projection_matrix);

//    if ( point.x() < 0 || point.x() > width() || point.y() < 0 || point.y() > height() )
//    {
//        point.setX(0);
//        point.setY(0);
//    }
//    else
//    {
//        uint32_t x = qMin( uint32_t(width()), uint32_t(p.x() + 0.5 * width()) );
//        uint32_t y = qMin( uint32_t(height()), uint32_t(p.y() + 0.5 * height()) );
//        point.setX(x);
//        point.setY(y);
//    }
    return point;
}

//QPoint Scene::normalize(QPoint p, int pixel)
//{
//    QPoint n;
//    n.setX( ( p.x() + width()/2 ) / width() /** pixel*/ );
//    n.setY( ( p.y() + height()/2 ) / height() /** pixel*/ );
//    return n;
//}

Point Scene::matrix_multiplicator(Point point, float matrix[4][4]) const
{
    Point camera_point;
    float w = matrix[0][3]*point.x() + matrix[1][3]*point.y() + matrix[2][3]*point.z() + matrix[3][3];
    camera_point.set_x((matrix[0][0]*point.x() + matrix[1][0]*point.y() + matrix[2][0]*point.z() + matrix[3][0]) / w);
    camera_point.set_y((matrix[0][1]*point.x() + matrix[1][1]*point.y() + matrix[2][1]*point.z() + matrix[3][1]) / w);
    camera_point.set_z((matrix[0][2]*point.x() + matrix[1][2]*point.y() + matrix[2][2]*point.z() + matrix[3][2]) / w);
    return camera_point;
}

void Scene::set_projection_matrix()
{
    float angle_of_view = 90;
    float scale = 1 / tan(angle_of_view / 2 * M_PI / 180);
    float far = 100; /*far clipping plane*/
    float near = 0.01; /*near clipping plane*/
    projection_matrix[0][0] = scale;
    projection_matrix[1][1] = scale;
    projection_matrix[2][2] = far / (far - near); /*remaping z*/
    projection_matrix[3][2] = far * near / (far - near); /*remaping z*/
    projection_matrix[2][3] = 1;
    projection_matrix[3][3] = 0;
}

QColor Scene::shading(Triangle triangle, QColor color, Point light)
{
    Point norm = triangle.normal();
    Point cent = triangle.center();
    Point light_vec = triangle.form_vector(light, cent);
    float angle = triangle.angle(norm, light_vec);
    if (angle > 90)
    {
        angle = 180 - angle;
    }
    double brightness = cos(double(angle) * M_PI / 180);
    float ambient = 0;
    double final_brightness = std::max(brightness, double(0)) + ambient;
    QColor result_color = apply_brightness(color, final_brightness);
    return result_color;
}

QColor Scene::apply_brightness(QColor color, double brightness)
{
    return QColor(int(color.red()*brightness), int(color.green()*brightness), int(color.blue()*brightness), color.alpha());
}

const qreal Scene::width() const
{
    return width_;
}

const qreal Scene::height() const
{
    return height_;
}

QColor Scene::random_color() const
{
    qsrand(time(0));
    return QColor(qrand()%255, qrand()%255, qrand()%255);
}
void Scene::add(Triangle triangle)
{
//    qDebug() << __PRETTY_FUNCTION__;
    QPolygon pol;
    QBrush brush;
    brush.setColor(shading(triangle, color_, light_source));
    brush.setStyle(Qt::SolidPattern);
    QPen pen(shading(triangle, color_, light_source));
    auto points_0 = switch_to_pixel(triangle.points_3D_[0]);
    auto points_1 = switch_to_pixel(triangle.points_3D_[1]);
    auto points_2 = switch_to_pixel(triangle.points_3D_[2]);
//    triangle.print();
    pol << QPoint(points_0) << QPoint(points_1) << QPoint(points_2);
    addPolygon(pol, pen, brush);

//    QPen pen_1(Qt::yellow);
//    brush.setStyle(Qt::SolidPattern);
//    auto point_light = switch_to_pixel(light_source);
//    qDebug() << "light_source:" << light_source.x() << "," << light_source.y() << "," << light_source.z();
//    qDebug() << "light_source on canvas:" << point_light.x() << "," << point_light.y();
//    auto center = switch_to_pixel(triangle.center());
//    addLine(center.x(), center.y(), point_light.x(), point_light.y(), pen_1);


//    pen.setColor(Qt::black);
//    auto normal = switch_to_pixel(triangle.move(triangle.normal(), triangle.center()));
//    auto center = switch_to_pixel(triangle.center());
//    addLine(center.x(), center.y(), normal.x(), normal.y(), pen);

}

void Scene::set_light_source(Point l)
{
    light_source = l;
}

void Scene::move_light(Point light, Point move_to)
{
    std::cout << "void Scene::move_light_source()" << std::endl;
    Point move_vector;
    move_vector.set_x(move_to.x() - light.x());
    move_vector.set_y(move_to.y() - light.y());
    move_vector.set_z(move_to.z() - light.z());
    move_vector.print();
    light_source.set_x(light_source.x() + move_vector.x());
    light_source.set_y(light_source.y() + move_vector.y());
    light_source.set_z(light_source.z() + move_vector.z());
}

Point Scene::move_point(Point move_point, Point move_to)
{
    Point point;
    point.set_x(move_to.x() - move_point.x());
    point.set_y(move_to.y() - move_point.y());
    point.set_z(move_to.z() - move_point.z());
    return point;
}






