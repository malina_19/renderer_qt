#include "model.h"

Model::Model()
{
    initialize_queue();
}

void Model::initialize_queue()
{
    std::ifstream my_file;
    my_file.open("/home/v/hello_world/utah_teapot");
    std::string line_str;
    int k = 50; /*scale of the teapot*/
    if (my_file.is_open())
    {
        while( std::getline( my_file, line_str ) )
                {
                    std::istringstream line( line_str );
                    std::string line_type;
                    line >> line_type;

                    // vertex
                    if( line_type == "v" )
                    {
                        float x = 0, y = 0, z = 0;
                        line >> x >> y >> z;
                        points.push_back( Point( x*k, y*(-k), z*k) );
                    }
                    // faces
                    if( line_type == "f" )
                    {
                        int a = 0, b = 0, c = 0;
                        line >> a >> b >> c;
                        q.push_back( Triangle(points[a - 1], points[b - 1], points[c - 1]) );
                    }
                 }
    }
    else { std::cout << "Couldn't open file\n"; }
//    std::sort(q.begin(), q.end(), [](Triangle q1, Triangle q2){ return q1.center().z() < q2.center().z(); });
}

Point Model::center()
{
    Point max, min;
    for (auto i : points)
    {
        Point p = i;
        if (p.x() > max.x())
        {
            max.set_x(p.x());
        }
        if (p.y() > max.y())
        {
            max.set_y(p.y());
        }
        if (p.z() > max.z())
        {
            max.set_z(p.z());
        }
        if (p.x() < min.x())
        {
            min.set_x(p.x());
        }
        if (p.y() < min.y())
        {
            min.set_y(p.y());
        }
        if (p.z() < min.z())
        {
            min.set_z(p.z());
        }
    }
    Point cent;
    cent.set_x((max.x() + min.x()) / 2);
    cent.set_y((max.y() + min.y()) / 2);
    cent.set_z((max.z() + min.z()) / 2);
    cent.print();
    return cent;
}

void Model::move(Point move_point, Point move_to)
{
    Triangle tr;
    for (int i=0; i < q.size(); i++)
    {
        if (i == 0)
        {
        std::cout << "#1----------------" << std::endl;
        q[i].print();
        }
        tr = q[i];
        tr.move(move_point, move_to);
        q[i] = tr;
        if (i == 0)
        {
        std::cout << "#2----------------" << std::endl;
        q[i].print();
        }
    }
}

void Model::sort_queue()
{
    std::sort(q.begin(), q.end(), [](Triangle q1, Triangle q2){ return q1.center().z() < q2.center().z(); });
}
