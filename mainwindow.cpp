﻿#include <string>
#include <cstdlib>

#include <QString>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new Scene(0, 0, 800, 800, this);
    ui->graphicsView->setScene(scene);
    model = new Model();
//    for (int i=0; i < model->q.size(); i++)
//    {
//        triangle_ = new Triangle((model->q[i].points_3D_[0]), (model->q[i].points_3D_[1]), (model->q[i].points_3D_[2]));
//        scene->add(*triangle_);
//    }
    light = scene->light_source;

    animationTimer = new QTimer(this);
    animationTimer->start(1000/10);
    connect(animationTimer, &QTimer::timeout, this, &MainWindow::rePaint);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::light_circle_test(Point center, float radius, float delta, int step)
{
    qDebug() << __PRETTY_FUNCTION__;
    float x = center.x() - radius + delta * step;
    float root = abs(pow(x - center.x(), 2) - pow(radius, 2));
    float y = sqrt(root) + center.y();
    Point p(x, y, 0);
    scene->move_light(scene->light_source, light);
    p.print();
    scene->clear();
    scene->move_light(light, p);
    scene->add(*triangle_);
//    QPoint a = scene->switch_to_pixel(center);
//    scene->addEllipse(a.x(), a.y(), radius*2, radius*2, QPen(Qt::black), QBrush(Qt::white, Qt::SolidPattern));
    scene->update();
}

void MainWindow::light_circle_test_1(Point center, float radius, float delta, int step)
{
    qDebug() << __PRETTY_FUNCTION__;
    float x = center.x() - radius + delta * step;
    float root = abs(pow(x - center.x(), 2) - pow(radius, 2));
    float y = center.y() - sqrt(root);
    Point p(x, y, 0);
    scene->move_light(scene->light_source, light);
    p.print();
    scene->clear();
    scene->move_light(light, p);
    scene->add(*triangle_);
    scene->update();
}

void MainWindow::pot_cirlce_test(Point center, float radius, float delta, int step)
{
    float x = center.x() - radius + delta * step;
    float root = abs(pow(x - center.x(), 2) - pow(radius, 2));
    float y = center.y() + sqrt(root);
    Point p(x, y, 0);
    scene->clear();
    model->move(model->center(), p);
    for (Triangle i : model->q)
    {
        scene->add(i);
    }
    scene->update();
}

void MainWindow::pot_cirlce_test_1(Point center, float radius, float delta, int step)
{
    float x = center.x() - radius + delta * step;
    float root = abs(pow(x - center.x(), 2) - pow(radius, 2));
    float y = center.y() - sqrt(root);
    Point p(x, y, 0);
    scene->clear();
    model->move(model->center(), p);
//    for (int i = 0; i < model->q.size(); i++)
    for (Triangle i : model->q)
    {
//        triangle_ = new Triangle((model->q[i].points_3D_[0]), (model->q[i].points_3D_[1]), (model->q[i].points_3D_[2]));
        scene->add(i);
    }
    scene->update();
}

void MainWindow::pot_around_axis_test(Point center, float delta, int step)
{
    Triangle tr;
    for (int i=0; i < model->q.size(); i++)
    {
        tr = model->q[i];
        float radius = pow( pow(tr.center().x() - center.x(), 2) + pow(tr.center().z() - center.z(), 2), 0.5);
        qDebug() << radius;
        float x = center.x() - radius + delta * step;
        float root = abs(pow(x - center.x(), 2) - pow(radius, 2));
        float z = center.z() + sqrt(root);
        Point p(x, 0, z);
        scene->clear();
        model->move(model->center(), p);
        model->q[i] = tr;
        scene->add(model->q[i]);
    }
    scene->update();
}

void MainWindow::create_picture(int step)
{
    scene->clearSelection();                                                  // Selections would also render to the file
//    With bounding rect:
//    scene->setSceneRect(scene->itemsBoundingRect());                          // Re-shrink the scene to it's bounding contents
//    QImage image(scene->sceneRect().size().toSize(), QImage::Format_ARGB32);  // Create the image with the exact size of the shrunk scene
//    Without bounding rect:
    QImage image(800,800,QImage::Format_RGB32);
    image.fill(Qt::transparent);                                             // Start all pixels transparent

    QPainter painter(&image);
    scene->render(&painter);
    QString file_name;
    if ((step >= 0) && (step <= 9))
    {
        file_name = QString::fromStdString("renderer0" + std::to_string(step) + ".png");
    }
    else
    {
        file_name = QString::fromStdString("renderer" + std::to_string(step) + ".png");
    }
    qDebug() << file_name;
    image.save(file_name);
}

void MainWindow::rePaint()
{
    qDebug() << __PRETTY_FUNCTION__;
    float delta = 5;
    float radius = 100;
    int steps = 2 * radius / delta;
    static int step = 0;
    if (step < steps)
    {
        qDebug() << "step" << step;
        pot_cirlce_test(Point(50, 80, 100), radius, delta, step);
//        pot_around_axis_test(Point(50, 80, 100), delta, step);
    }
    qDebug() << "________________________________________________________________________________________________";
    if (step >= steps)
    {
        static int i = 0;
        int step_rev = steps - i;
        qDebug() << "step_rev" << step_rev;
        if (step_rev >= 0)
        {
            pot_cirlce_test_1(Point(50, 80, 100), radius, delta, step_rev);
            i++;
        }
        if (i > steps)
        {
            i = 0;
        }
    }
    step++;
    if (step > 2 * steps)
    {
        step = 0;
    }
    create_picture(step);
}

//void MainWindow::rePaint()
//{
//    static auto i = 0;
//    if (i <= scene->q.size())
//    {
//        triangle_ = new Triangle((scene->q[i].points_3D_[0]), (scene->q[i].points_3D_[1]), (scene->q[i].points_3D_[2]));
//        scene->add(*triangle_);
//        scene->update();
//        i++;
//    }

//}








