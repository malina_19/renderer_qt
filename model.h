#ifndef MODEL_H
#define MODEL_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "triangle.h"

class Model : public Triangle
{
public:
    Model();
    ~Model();
    void initialize_queue();
    Point center();
    void move(Point move_point, Point move_to);
    void sort_queue();
public:
    std::vector<Triangle> q;
private:
    std::vector<Point>points;
};

#endif // MODEL_H
