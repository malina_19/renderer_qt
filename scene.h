#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPainter>
#include <QBrush>

#include <QDebug>

#include "triangle.h"
#include "model.h"

class Scene : public QGraphicsScene
{
public:
    Scene();
    Scene(qreal x, qreal y, qreal width, qreal height, QObject *parent = nullptr);
    void add(Triangle triangle);
    void set_light_source(Point l);
    void move_light(Point light, Point move_to);
    Point move_point(Point move_point, Point move_to);
//    void initialize_queue();
//    void sort_queue();
//    QPoint normalize(QPoint p, int pixel);
public:
    Point light_source;
//    std::vector <Triangle> q;
private:
    const qreal width() const;
    const qreal height() const;
    QColor random_color() const;
    QPoint switch_to_pixel(Point p);
    Point matrix_multiplicator(Point point, float matrix[4][4]) const;
    void set_projection_matrix();
    QColor shading(Triangle triangle, QColor color, Point light);
    QColor apply_brightness(QColor color, double brightness);
private:
    Point camera;
    Point plane;
    qreal width_, height_;
    QColor color_;
    float projection_matrix[4][4] = {0};

};

#endif // SCENE_H
