﻿#include "triangle.h"

Triangle::Triangle()
{
    points_3D_[0] = Point(0, 0, 0);
    points_3D_[1] = Point(0, 0, 0);
    points_3D_[2] = Point(0, 0, 0);
}

Triangle::Triangle(Point point1, Point point2, Point point3)
{
    points_3D_[0] = point1;
    points_3D_[1] = point2;
    points_3D_[2] = point3;
    points_3D[0] = points_3D_[0];
    points_3D[1] = points_3D_[1];
    points_3D[2] = points_3D_[2];
}

//Triangle::Triangle(Triangle * other)
//{
//    qDebug() << __PRETTY_FUNCTION__;
//    points_3D_[0] = other->points_3D_[0];
//    points_3D_[1] = other->points_3D_[1];
//    points_3D_[2] = other->points_3D_[2];

//    points[0] = Switch_to_pixel(points_3D_[0]);
//    points[1] = Switch_to_pixel(points_3D_[1]);
//    points[2] = Switch_to_pixel(points_3D_[2]);

//    qDebug() << points_3D_[0];
//    qDebug() << points_3D_[1];
//    qDebug() << points_3D_[2];

//    qDebug() << points[0];
//    qDebug() << points[1];
//    qDebug() << points[2];
//}

Triangle::~Triangle()
{
//    std::cout << "Dectructor worked!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
}

void Triangle::print()
{
    std::cout << "points_3D_[0] = " << points_3D_[0].x() << ", " << points_3D_[0].y() << ", " << points_3D_[0].z() << std::endl;
    std::cout << "points_3D_[1] = " << points_3D_[1].x() << ", " << points_3D_[1].y() << ", " << points_3D_[1].z() << std::endl;
    std::cout << "points_3D_[0] = " << points_3D_[2].x() << ", " << points_3D_[2].y() << ", " << points_3D_[2].z() << std::endl;
}

//void Triangle::rotate_around_z(double angle)
//{
//    total_angle_= total_angle_ + angle;
//    if (round(total_angle_ / (2*360)) == 1)
//    {
//        points_3D_[0] = points_3D[0];
//        points_3D_[1] = points_3D[1];
//        points_3D_[2] = points_3D[2];
//        total_angle_ = 0;
//    }
//    angle = angle * M_PI / 180;
//    float qx, qy;
//    Point point;
//    point = points_3D_[0];
//    qx = cos(angle)*point.x() - sin(angle)*point.y();
//    qy = sin(angle)*point.x() + cos(angle)*point.y();
//    points_3D_[0].set_x(qx);
//    points_3D_[0].set_y(qy);
//    points_3D_[0].set_z(point.z());
//    point = points_3D_[1];
//    qx = cos(angle)*point.x() - sin(angle)*point.y();
//    qy = sin(angle)*point.x() + cos(angle)*point.y();
//    points_3D_[1].set_x(qx);
//    points_3D_[1].set_y(qy);
//    points_3D_[1].set_z(point.z());
//    point = points_3D_[2];
//    qx = cos(angle)*point.x() - sin(angle)*point.y();
//    qy = sin(angle)*point.x() + cos(angle)*point.y();
//    points_3D_[2].set_x(qx);
//    points_3D_[2].set_y(qy);
//    points_3D_[2].set_z(point.z());
//    std::cout << total_angle_ << std::endl;

//}

//void Triangle::rotate_around_y(double angle)
//{
//    total_angle_= total_angle_ + angle;
//    if ( round(total_angle_ / (2*360)) == 1 )
//    {
//        points_3D_[0] = points_3D[0];
//        points_3D_[1] = points_3D[1];
//        points_3D_[2] = points_3D[2];
//        total_angle_ = 0;
//    }
//    angle = angle * M_PI / 180;
//    float qx, qz;
//    Point point;
//    point = points_3D_[0];
//    qx = cos(angle)*point.x() + sin(angle)*point.z();
//    qz = -sin(angle)*point.x() + cos(angle)*point.z();
//    points_3D_[0].set_x(qx);
//    points_3D_[0].set_y(point.y());
//    points_3D_[0].set_z(qz);
//    point = points_3D_[1];
//    qx = cos(angle)*point.x() + sin(angle)*point.z();
//    qz = -sin(angle)*point.x() + cos(angle)*point.z();
//    points_3D_[1].set_x(qx);
//    points_3D_[1].set_y(point.y());
//    points_3D_[1].set_z(qz);
//    point = points_3D_[2];
//    qx = cos(angle)*point.x() + sin(angle)*point.z();
//    qz = -sin(angle)*point.x() + cos(angle)*point.z();
//    points_3D_[2].set_x(qx);
//    points_3D_[2].set_y(point.y());
//    points_3D_[2].set_z(qz);
//}

//void Triangle::rotate_around_x(double angle)
//{
//    total_angle_= total_angle_ + angle;
//    if (round(total_angle_ / (2*360)) == 1)
//    {
//        points_3D_[0] = points_3D[0];
//        points_3D_[1] = points_3D[1];
//        points_3D_[2] = points_3D[2];
//        total_angle_ = 0;
//    }
//    angle = angle * M_PI / 180;
//    float qy, qz;
//    Point point;
//    point = points_3D_[0];
//    qy = cos(angle)*point.y() - sin(angle)*point.z();
//    qz = sin(angle)*point.y() + cos(angle)*point.z();
//    points_3D_[0].set_x(point.x());
//    points_3D_[0].set_y(qy);
//    points_3D_[0].set_z(qz);
//    point = points_3D_[1];
//    qy = cos(angle)*point.y() - sin(angle)*point.z();
//    qz = sin(angle)*point.y() + cos(angle)*point.z();
//    points_3D_[1].set_x(point.x());
//    points_3D_[1].set_y(qy);
//    points_3D_[1].set_z(qz);
//    point = points_3D_[2];
//    qy = cos(angle)*point.y() - sin(angle)*point.z();
//    qz = sin(angle)*point.y() + cos(angle)*point.z();
//    points_3D_[2].set_x(point.x());
//    points_3D_[2].set_y(qy);
//    points_3D_[2].set_z(qz);
//}

Point Triangle::cross_product(Point v_1, Point v_2)
{
    Point product;
    product.set_x( (v_1.y() * v_2.z()) - (v_1.z() * v_2.y()) );
    product.set_y( (v_1.z() * v_2.x()) - (v_1.x() * v_2.z()) );
    product.set_z( (v_1.x() * v_2.y()) - (v_1.y() * v_2.x()) );
    return product;
}

Point Triangle::normalize_vector(Point vec)
{
    float x = vec.x();
    float y = vec.y();
    float z = vec.z();
    float length = sqrt(x*x + y*y + z*z);
    x = x / length;
    y = y / length;
    z = z / length;
    return vec;
}

float Triangle::angle(Point vec1, Point vec2)
{
    vec1 = normalize_vector(vec1);
    vec2 = normalize_vector(vec2);
    double dot_product = (vec1.x()*vec2.x() + vec1.y()*vec2.y() + vec1.z()*vec2.z());
    double magnitude1 = sqrt(pow(vec1.x(), 2) + pow(vec1.y(), 2) + pow(vec1.z(), 2));
    double magnitude2 = sqrt(pow(vec2.x(), 2) + pow(vec2.y(), 2) + pow(vec2.z(), 2));
    double alpha = dot_product / (magnitude1 * magnitude2);
    float angle = 180 / M_PI * acos(alpha);
//    std::cout << "dot_product" << dot_product << std::endl;
//    std::cout << "magnitude1" << magnitude1 << std::endl;
//    std::cout << "magnitude2" << magnitude2 << std::endl;
//    std::cout << alpha << "//" << std::endl;
//    std::cout << angle << "//" << std::endl;
    return angle;
}

void Triangle::move(Point move_point, Point move_to)
{
//    std::cout << "void Triangle::move()" << std::endl;
    Point move_vector;
    move_vector = form_vector(move_to, move_point);
//    move_vector.print();
    points_3D_[0].set_x(points_3D[0].x() + move_vector.x());
    points_3D_[0].set_y(points_3D[0].y() + move_vector.y());
    points_3D_[0].set_z(points_3D[0].z() + move_vector.z());

    points_3D_[1].set_x(points_3D[1].x() + move_vector.x());
    points_3D_[1].set_y(points_3D[1].y() + move_vector.y());
    points_3D_[1].set_z(points_3D[1].z() + move_vector.z());

    points_3D_[2].set_x(points_3D[2].x() + move_vector.x());
    points_3D_[2].set_y(points_3D[2].y() + move_vector.y());
    points_3D_[2].set_z(points_3D[2].z() + move_vector.z());
}

Point Triangle::normal()
{
    Point p[3];
    p[0] = points_3D_[0];
    p[1] = points_3D_[1];
    p[2] = points_3D_[2];
    Point vector_1, vector_2;

    Point v1(p[0].x(),  p[0].y(), p[0].z());
    Point v2(p[1].x(),  p[1].y(), p[1].z());
    Point v3(p[2].x(),  p[2].y(), p[2].z());

    vector_1.set_x(v3.x() - v1.x());
    vector_1.set_y(v3.y() - v1.y());
    vector_1.set_z(v3.z() - v1.z());

    vector_2.set_x(v2.x() - v1.x());
    vector_2.set_y(v2.y() - v1.y());
    vector_2.set_z(v2.z() - v1.z());

    normal_ = cross_product(vector_1,vector_2);
    return normal_;
}

Point Triangle::center()
{
    float sx, sy, sz, sarea;
    sx = sy = sz = sarea = 0;
    float x1 = points_3D_[0].x();
    float y1 = points_3D_[0].y();
    float z1 = points_3D_[0].z();
    float x2 = points_3D_[1].x();
    float y2 = points_3D_[1].y();
    float z2 = points_3D_[1].z();
    float x3 = points_3D_[2].x();
    float y3 = points_3D_[2].y();
    float z3 = points_3D_[2].z();
    float dx1 = x3 - x1;
    float dy1 = y3 - y1;
    float dz1 = z3 - z1;
    float dx2 = x3 - x2;
    float dy2 = y3 - y2;
    float dz2 = z3 - z2;
    float cpx = dy1*dz2 - dz1*dy2;
    float cpy = dz1*dx2 - dx1*dz2;
    float cpz = dx1*dy2 - dy1*dx2;
    float area = sqrt(cpx*cpx + cpy*cpy + cpz*cpz)/2;
    sx = sx + (x1 + x2 + x3)/3*area;
    sy = sy + (y1 + y2 + y3)/3*area;
    sz = sz + (z1 + z2 + z3)/3*area;
    sarea = sarea + area;
    float cx = sx/sarea;
    float cy = sy/sarea;
    float cz = sz/sarea;
//    float cx = (points_3D_[0].x() + points_3D_[1].x() + points_3D_[2].x()) / 3;
//    float cy = (points_3D_[0].y() + points_3D_[1].y() + points_3D_[2].y()) / 3;
//    float cz = (points_3D_[0].z() + points_3D_[1].z() + points_3D_[2].z()) / 3;
    center_.set_x(cx);
    center_.set_y(cy);
    center_.set_z(cz);
    return center_;
}

//Point Triangle::form_vector(Point terminal, Point initial)
//{
//    Point vec;
//    vec.set_x(terminal.x() - initial.x());
//    vec.set_y(terminal.y() - initial.y());
//    vec.set_z(terminal.z() - initial.z());
//    return vec;
//}


